import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pessoa } from '../models/pessoa';


@Injectable({
  providedIn: 'root'
})

export class PessoaService {
  url = 'http://localhost:35322/api/pessoas';
  constructor(private http: HttpClient) { }
  GetPessoas(): Observable<Pessoa[]> {
    return this.http.get<Pessoa[]>(this.url);

  }
  getPessoaById(idPessoa: number): Observable<Pessoa> {
    return this.http.get<Pessoa>(this.url + '/' + idPessoa);
  }
  criarPessoa(pessoa: Pessoa): Observable<Pessoa> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.post<Pessoa>(this.url,
    pessoa, httpOptions);
  }
  updatePessoa(idPessoa: number, pessoa: Pessoa): Observable<number> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.put<number>(this.url + '/' + idPessoa, pessoa,
    httpOptions);
  }
  deletePessoaById(idPessoa: number): Observable<number> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.delete<number>(this.url + '/' + idPessoa,
      httpOptions);
  }
}
