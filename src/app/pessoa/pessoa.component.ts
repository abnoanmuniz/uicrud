import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { PessoaService } from '../service/pessoa.service';
import { Pessoa } from "../models/pessoa";

@Component({
  selector: "app-pessoa",
  templateUrl: "./pessoa.component.html",
  styleUrls: ["./pessoa.component.css"]
})
export class PessoaComponent implements OnInit {
  dataSaved = false;
  pessoaForm: any;
  todasPessoas: Pessoa[];
  pessoaIdAtualizar = null;
  message = null;
  radioValue: number;

  constructor(
    private formbulider: FormBuilder,
    private pessoaService: PessoaService
  ) {}

  ngOnInit() {
    this.pessoaForm = this.formbulider.group({
      nome: ["", [Validators.required]],
      dataNascimento: ["", [Validators.required]],
      email: ["", [Validators.required]],
      genero: ["", [Validators.required]],
      endereco: ["", [Validators.required]],
      cpf: ["", [Validators.required]]
    });
    this.loadTodos();
  }
  loadTodos() {
    this.pessoaService.GetPessoas().subscribe((pessoas: Pessoa[]) => {
      this.todasPessoas = pessoas;
    });
  }
  onFormSubmit() {
    this.dataSaved = false;
    const pessoa = this.pessoaForm.value;
    this.CreatePessoa(pessoa);
    this.pessoaForm.reset();
  }
  loadPessoaToEdit(id: number) {
    this.pessoaService.getPessoaById(id).subscribe(pessoa => {
      this.message = null;
      this.dataSaved = false;
      this.pessoaIdAtualizar = pessoa.id;
      this.pessoaForm.controls["nome"].setValue(pessoa.nome);
      this.pessoaForm.controls["dataNascimento"].setValue(
        pessoa.dataNascimento
      );
      this.pessoaForm.controls["email"].setValue(pessoa.email);
      this.radioValue = pessoa.genero;
      this.pessoaForm.controls["endereco"].setValue(pessoa.endereco);
      this.pessoaForm.controls["cpf"].setValue(pessoa.cpf);
    });
  }
  CreatePessoa(pessoa: Pessoa) {
    if (this.pessoaIdAtualizar == null) {
      this.pessoaService.criarPessoa(pessoa).subscribe(() => {
        this.dataSaved = true;
        this.message = "Registro salvo com sucesso!";
        this.loadTodos();
        this.pessoaIdAtualizar = null;
        this.pessoaForm.reset();
      });
    } else {
      pessoa.id = this.pessoaIdAtualizar;
      this.pessoaService.updatePessoa(pessoa.id, pessoa).subscribe(() => {
        this.dataSaved = true;
        this.message = "Registro salvo com sucesso!";
        this.loadTodos();
        this.pessoaIdAtualizar = null;
        this.pessoaForm.reset();
      });
    }
  }
  deletePessoa(id: number) {
    if (confirm("Você tem certeza que quer deletar este registro ?")) {
      this.pessoaService.deletePessoaById(id).subscribe(() => {
        this.dataSaved = true;
        this.message = "Registro deletado!";
        this.loadTodos();
        this.pessoaIdAtualizar = null;
        this.pessoaForm.reset();
      });
    }
  }
  resetForm() {
    this.pessoaForm.reset();
    this.message = null;
    this.dataSaved = false;
  }
}
