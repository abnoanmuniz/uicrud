export class Pessoa {
    id: number;
    nome: string;
    cpf: number;
    dataNascimento: Date;
    email: string;
    genero: number;
    endereco: string;
}
